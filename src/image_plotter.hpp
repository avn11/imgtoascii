#include <string>
#include <vector>


/**
* @brief Class for wrawing an image as ascii characters to stdout.
*/
class image_plotter
{
  private:
    std::vector< std::vector<char> > _canvas;  ///< The canvas we draw on
    unsigned int _rows;                        ///< Number of rows
    unsigned int _cols;                        ///< Number of columns
    static constexpr float col2row_ratio = 2.3f;
    /**
    * @brief Set new size and clear the canvas
    *
    * @param rows  Number of rows
    * @param cols  Number of cols
    */
    void set_canvas(const int irows,
                    const int icols)
    {
      float rowfac = float(irows) / float(_rows);
      _cols = (icols / rowfac) * col2row_ratio;
      _canvas.resize(_rows);

      this->reset();
    }

    /**
    * @brief Draw image onto the canvas.
    *
    * @param imgdata  The image data.
    * @param irows    The number of image rows.
    * @param icols    The number of image cols.
    * @param chars    The characters to use to represent the color values (dark to bright).
    */
    void draw_image(const unsigned char* imgdata,
                    const int irows,
                    const int icols,
                    const std::string & chars)
    {
      int rowdiv = (irows + _rows - 1) / _rows;
      int coldiv = (icols + _cols - 1) / _cols;

      std::vector<unsigned int> avrg_cnt(_cols*_rows, 0),
                                avrg_val(_cols*_rows, 0);

      for(int r=0; r < irows; r++)
        for(int c=0; c < icols; c++) {
          int wr = r / rowdiv;
          int wc = c / coldiv;
          avrg_val[wr*_cols+wc] += imgdata[r*icols+c];
          avrg_cnt[wr*_cols+wc]++;
        }

      int valdiv = (255 + chars.size() - 1) / chars.size();
      for(int r=0; r < _rows; r++)
        for(int c=0; c < _cols; c++) {
          if(avrg_cnt[r*_cols+c]) {
            int idx = (avrg_val[r*_cols+c] / avrg_cnt[r*_cols+c]) / valdiv;
            _canvas[r][c] = chars[idx];
          }
        }
    }


  public:
    /// empty constructor
    image_plotter() : _rows(0), _cols(0)
    {}

    /**
    * @brief Constructor specifying the canvas size
    *
    * @param rows  Number of rows
    */
    image_plotter(const unsigned int rows) : _rows(rows)
    {}

    /// set num rows. cols are computed internally to preserve
    /// image aspect ratio
    void set_rows(int rows)
    {
      _rows = rows;
    }

    /**
    * @brief Clear the canvas
    */
    void reset()
    {
      for(unsigned int i=0; i<_rows; i++) {
        _canvas[i].assign(_cols + 1, ' ');
        _canvas[i][_cols] = '\0';
      }
    }

    /**
    * @brief Print an image to stdout.
    *
    * @param imgdata  The image data.
    * @param irows    The number of image rows.
    * @param icols    The number of image cols.
    * @param chars    The characters to use to represent the color values (dark to bright).
    */
    void print(const unsigned char* imgdata,
               const int irows,
               const int icols,
               const std::string & chars)
    {
      if(!_rows) _rows = 40;
      this->set_canvas(irows, icols);

      draw_image(imgdata, irows, icols, chars);

      for(size_t i=0; i<_rows; i++) {
          printf("   %s\n", _canvas[i].data());
      }
    }
};
