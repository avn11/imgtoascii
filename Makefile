CXX = g++
CXXFLAGS = -O0 -g
INC = -Isrc

imgToAscii: main.cpp
	$(CXX) $(CXXFLAGS) $(INC) -o $@ $<

clean:
	rm -f imgToAscii
