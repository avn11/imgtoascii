#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

#include "image_plotter.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


int main(int argc, char** argv)
{
  if(argc < 3) {
    fprintf(stderr, "Wrong usage. Use %s imgfile chars [numrows]\n", argv[0]);
    exit(1);
  }

  std::string imgfile = argv[1];
  std::string chars = argv[2];

  int numrows = 0;
  if(argc == 4)
    numrows = atoi(argv[3]);
  else
    numrows = 40;

  int x, y, n;
  unsigned char* imgdata = stbi_load(imgfile.c_str(), &x, &y, &n, 1);

  printf("Loaded image.\n"
         "Dimensions: x = %d, y = %d, nchannes = %d\n", x, y, n);

  // make sure y isnt smaller than numrows
  numrows = y < numrows ? y : numrows;

  image_plotter ip(numrows);
  ip.print(imgdata, y, x, chars);

  free(imgdata);

  return 0;
}
